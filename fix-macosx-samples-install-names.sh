#!/bin/sh -e

NITE_VERSION_SUFFIX=1_5_2

run ()
{
    echo " $@"
    "$@"
}

if [ $# -lt 2 ]; then
	echo "Usage: $0 <bin_dir> <sample> [...]"
	exit 1
fi

bin_dir="$1"; shift
samples="$@"

fix_names () # sample
{
	sample="$1"

	run install_name_tool -change ../../Bin/x86-Release/libXnVNite_$NITE_VERSION_SUFFIX.dylib @loader_path/../lib/libXnVNite_$NITE_VERSION_SUFFIX.dylib "$bin_dir/$sample"
}

for sample in $samples; do
	fix_names $sample
done
